FROM php:5.6.18-fpm
# Install modules
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libxml2-dev \
        unzip \
	    git \
        mariadb-client \
    && docker-php-ext-install mcrypt pdo_mysql mysql mysqli mbstring opcache soap \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

CMD ["php-fpm"]
